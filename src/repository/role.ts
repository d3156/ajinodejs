import {EntityRepository, Repository} from "typeorm";
import {Shoes} from "../entity/shoes";
@EntityRepository(Shoes)
export class ShoesRepository extends Repository<Shoes> {

}
