import {module} from "../decorator/module";
import {del, get, post, put} from "../decorator/route";
import {getCustomRepository, Like} from "typeorm";
// import {UserRepository} from "../repository/user";
// import {User} from "../entity/user";
import {Context} from "koa";
import {ShoesRepository} from "../repository/role";
import {Shoes} from "../entity/shoes";

@module("/users")
export default class UserModule {
    @get("/", [])
    public async get(ctx: Context): Promise<void> {
        const shoesRepository = getCustomRepository(ShoesRepository);
        if (ctx.request.query.brand) {
            const dataItem = await shoesRepository.find({
                where: {brand: Like(`%${ctx.request.query.brand}`)},
                order: {brand: "ASC"},
            });

            ctx.body = {
                status: 100,
                message: "Success Get Data",
                result: dataItem,
            };
        } else {
            const dataItem = await shoesRepository.find()
                .catch((err) => {
                    console.log("Err", err);

                    throw err;
                })

            ctx.body = {
                status: 100,
                message: "Success create",
                results: dataItem,
            };
        }
    }

    @get("/:id", [])
    public async getById(ctx: Context,): Promise<void> {
        const shoesRepository = getCustomRepository(ShoesRepository);
        const item = await shoesRepository.findOne(ctx.params.id)
            .catch((err)=> {
                console.log("Err", err);

                throw err;
            })

        ctx.body = {
            status: 100,
            message: "Success Get Data",
            results: item,
        };
    }

    @post("/create", [], Shoes)
    public async create(ctx: Context, body: Shoes): Promise<void> {
        ctx.log.info("Started");

        const shoesRepository = getCustomRepository(ShoesRepository);

        await shoesRepository.insert(body);

        ctx.log.info("new_user_created");
        // throw new Error("InvalidRequestyomaiersnteiqwfiopeqiowf");

        ctx.body = {
            message: "Success Post Data",
            result: body,
        };
    }


    @put("/:id", [], Shoes)
    public async put(ctx: Context, body: Shoes): Promise<void> {
        const shoesRepository = getCustomRepository(ShoesRepository);

        const exsisShoes = await shoesRepository.findOne(ctx.params.id);

        Object.assign(exsisShoes, body);

        await shoesRepository.save(exsisShoes);

        ctx.body = {
            status: 100,
            message: "Success Update Data",
            result: exsisShoes,
        };
    }

    @del("/:id", [], Shoes)
    public async del(ctx: Context): Promise<void> {
        const shoesRepository = getCustomRepository(ShoesRepository);

        const reject = await shoesRepository.findOne(ctx.params.id);

        await shoesRepository.delete(reject);

        ctx.body = {
            status: 100,
            message: "success Delete Data",
            result: ctx.params.id
        };
    }
}
