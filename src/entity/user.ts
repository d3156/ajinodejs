import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Field, ID, ObjectType} from "type-graphql";
import {Contains} from "class-validator";
import {Body, RequestBody} from "../interfaces/request";
import {pick} from "lodash";

@Entity()
@ObjectType()
export class User implements RequestBody {
    @Field(() => ID)
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Contains("hello")
    @Field(() => String)
    @Column()
    firstName: string;

    @Field(() => String)
    @Column()
    lastName: string;

    fromJson(data: Body): void {
        Object
            .keys(pick(data, ["firstName", "lastName"]))
            .forEach((key) => {
                // eslint-disable-next-line security/detect-object-injection
                this[key] = data[key];
            });
    }
}
