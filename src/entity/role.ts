import {Field, ID} from "type-graphql";
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Body, RequestBody} from "../interfaces/request";
import {pick} from "lodash";

@Entity()
export class Role implements RequestBody{
    @Field(()=>ID)
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        length: 100,
    })
    name: string;

    @Column("numeric")
    age: number;

    fromJson(data: Body): void {
        Object
            .keys(pick(data, ["id","name", "age"]))
            .forEach((key) => {
                // eslint-disable-next-line security/detect-object-injection
                this[key] = data[key];
            });
    }
}
