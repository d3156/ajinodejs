import {Field, ID} from "type-graphql";
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Body, RequestBody} from "../interfaces/request";
import {pick} from "lodash";

@Entity()
export class Shoes implements RequestBody{
    @Field(()=>ID)
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        length: 100,
    })
    brand: string;

    @Column({
        length: 150,
    })
    model: string;

    @Column({
        length: 10,
    })
    type: string;

    @Column({
        scale: 250,
    })
    stock: number;

    fromJson(data: Body): void {
        Object
            .keys(pick(data, ["id", "brand", "model", "type", "stock"]))
            .forEach((key) => {
                // eslint-disable-next-line security/detect-object-injection
                this[key] = data[key];
            });
    }
}
